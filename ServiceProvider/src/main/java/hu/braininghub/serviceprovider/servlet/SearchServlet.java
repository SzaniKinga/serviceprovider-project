package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.AnonymServiceDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;

import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SearchServlet", urlPatterns = {"/SearchServlet"})
public class SearchServlet extends HttpServlet {

    @Inject
    ServiceService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        String searchedWord = request.getParameter("search");
        if (searchedWord == null) {
           response.sendRedirect("DemoServlet");
        } else if (user == null) {

            List<AnonymServiceDTO> filteredList = service.searchBoxForAnonymUsers(searchedWord);
            if (!filteredList.isEmpty()) {
                
                for (AnonymServiceDTO dto : filteredList) {
                    if (dto.getImage().equals("")) {
                        dto.setImage("images/placeholder.jpg");
                    }
                }

                request.setAttribute("filteredList", filteredList);
                request.getRequestDispatcher("WEB-INF/searchedCards.jsp").forward(request, response);

            } else {
                request.getRequestDispatcher("WEB-INF/noServices.jsp").forward(request, response);
            }

        } else {

            List<ServiceDTO> filteredList = service.searchBox(searchedWord);

            if (!filteredList.isEmpty()) {
                
                for (ServiceDTO dto : filteredList) {
                    if (dto.getImage().equals("")) {
                        dto.setImage("images/placeholder.jpg");
                    }
                }
                
                request.setAttribute("filteredList", filteredList);
                request.getRequestDispatcher("WEB-INF/afterSearchCards.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("WEB-INF/noServices.jsp").forward(request, response);

            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
