package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.CategoryService;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.LoggerFactory;

@WebServlet(name = "CategoryServlet", urlPatterns = {"/CategoryServlet"})
public class CategoryServlet extends HttpServlet {

    @Inject
    ServiceService service;

    @Inject
    CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        String clickedCategory = request.getParameter("category");
        CategoryDTO category = categoryService.getCategoryByName(clickedCategory);

        if (user == null) {
            response.sendRedirect("LoginServlet");
        } else if (clickedCategory == null) {
            response.sendRedirect("DemoServlet");
        } else {
            try {
                List<CategoryDTO> categories = categoryService.getCategories();

                if (!categories.contains(category)) {
                session.setAttribute("categoryError", "Invalid category!");
                response.sendRedirect(request.getContextPath() + "/DemoServlet");
                } else {
                    List<ServiceDTO> dtos = categoryService.findAllServicesFromSameCategory(category);
                    for (ServiceDTO dto : dtos) {
                        if (dto.getImage().equals("")) {
                            dto.setImage("images/placeholder.jpg");
                        }
                    }

                    request.setAttribute("listedServicesByCategory", dtos);
                    request.getRequestDispatcher("WEB-INF/listCardsByCategory.jsp").forward(request, response);
                }

            } catch (SQLException e) {
                LoggerFactory.getLogger(this.getClass().getName()).error("Database problems occured.");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        categoryService.addCategory(name);

        response.sendRedirect("ServiceServlet");

    }

}
