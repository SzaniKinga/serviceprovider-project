package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.helper.UploadHelper;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.CategoryService;
import hu.braininghub.serviceprovider.service.ProviderService;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.slf4j.LoggerFactory;

@WebServlet(name = "ServiceServlet", urlPatterns = {"/ServiceServlet"})
@MultipartConfig
public class ServiceServlet extends HttpServlet {

    @Inject
    private ServiceService serviceService;

    @Inject
    private CategoryService categoryService;

    @Inject
    private ProviderService providerService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserDTO loggedInUser = SessionHelper.getUserFromSession(session);

        if (loggedInUser == null) {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        } else {
            String providerEmail = loggedInUser.getEmail();
            ProviderDTO providerDTO = providerService.getProviderByEmail(providerEmail);
            if (providerDTO == null) {
                response.sendRedirect(request.getContextPath() + "/ProviderServlet");
            } else {
                try {
                    request.setAttribute("categoryDTOs", categoryService.getCategories());
                } catch (SQLException ex) {
                    LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred");
                }
                request.getRequestDispatcher("WEB-INF/addService.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("name", request.getParameter("name"));
        request.setAttribute("shortDescription", request.getParameter("shortDescription"));
        request.setAttribute("description", request.getParameter("description"));
        
        String[] categories = request.getParameterValues("category[]");
        request.setAttribute("categories", categories);

        try {
            request.setAttribute("categoryDTOs", categoryService.getCategories());
        } catch (SQLException ex) {
            Logger.getLogger(ServiceServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String nextElement = parameterNames.nextElement();

            if (!nextElement.equals("file") && (request.getParameter(nextElement) == null
                    || request.getParameter(nextElement).isEmpty())) {
                request.setAttribute("addServiceError", nextElement + " cannot be null or empty");
                request.getRequestDispatcher("WEB-INF/addService.jsp").forward(request, response);
                break;
            }
        }

        if (request.getAttribute("addServiceError") == null) {

            try {

                String name = request.getParameter("name");
                String shortDescription = request.getParameter("shortDescription");
                String description = request.getParameter("description");

                final String realPath = getServletContext().getRealPath("/");
                Part filePart = request.getPart("file");

                String pathToPersist = "";

                if (!filePart.getSubmittedFileName().equals("")) {

                    UploadHelper uploadHelper = new UploadHelper();

                    pathToPersist = uploadHelper.uploadFile(realPath, filePart);
                }
                List<CategoryDTO> categoryDTOs = new ArrayList<>();

                for (String category : categories) {
                    CategoryDTO categoryDTO = categoryService.getCategoryById(Integer.valueOf(category));
                    categoryDTOs.add(categoryDTO);
                }

                HttpSession session = request.getSession();
                UserDTO loggedInUser = SessionHelper.getUserFromSession(session);
                String providerEmail = loggedInUser.getEmail();
                ProviderDTO providerDTO = providerService.getProviderByEmail(providerEmail);

                serviceService.addService(name, shortDescription, description, pathToPersist, providerDTO, categoryDTOs);

            } catch (Exception ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred");
                request.setAttribute("addServiceError", ex.getMessage());
                request.getRequestDispatcher("WEB-INF/addService.jsp").forward(request, response);
                return;
            }
        } else {
            request.getRequestDispatcher("WEB-INF/addService.jsp").forward(request, response);
        }

        response.sendRedirect("ListServicesServlet");
    }

}
