package hu.braininghub.serviceprovider.mapper;

import hu.braininghub.serviceprovider.entity.UserEntity;
import hu.braininghub.serviceprovider.model.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    
    UserMapper MAPPER = Mappers.getMapper(UserMapper.class);
    
    UserDTO toDTO(UserEntity userEntity);
    
    UserEntity toEntity(UserDTO userDTO);
}
