package hu.braininghub.serviceprovider.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@EntityListeners(MessageEntityListener.class)
@Table(name = "messages")
public class MessageEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private LocalDateTime timeOfSending;
    
    @Column
    private String senderName;
    
    @Column
    private String sender;
    
    @Column
    private String recipientName;
    
    @Column
    private String recipient;
    
    @Column
    private String subject;
    
    @Column(length = 1024)
    private String message;
}
