package hu.braininghub.serviceprovider.model;

import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.slf4j.LoggerFactory;

@AllArgsConstructor
@NoArgsConstructor
@Singleton
public class ServiceDAO {

    @PersistenceContext
    private EntityManager em;

    public void addNewService(ServiceEntity service) {

        try {
            em.persist(service);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Service successfully created");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error creating new service. Reason: {}", e.getMessage());
        }
    }

    public void updateService(ServiceEntity service) {

        try {
            em.merge(service);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Service successfully updated");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error updating service. Reason: {}", e.getMessage());
        }
    }

    public void removeService(int id) {
        ServiceEntity s = this.getServiceById(id);
        try {
            em.remove(s);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Service successfully removed");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("An error occured during service removal. Reason: {}", e.getMessage());
            throw e;
        }
    }

    public ServiceEntity getServiceById(int id) {
        try {
            ServiceEntity s = em.find(ServiceEntity.class, id);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Service successfully found by {} id", id);
            return s;
        } catch (NoResultException e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Service could not be found. Reason: {}", e.getMessage());
            return null;
        }
    }

    public List<ServiceEntity> getServices() {
        List<ServiceEntity> services = em.createQuery("SELECT s FROM ServiceEntity s").getResultList();

        return services;
    }

    public List<ServiceEntity> getServicesOfProvider(ProviderEntity providerEntity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(ServiceEntity.class);

        Root<ServiceEntity> service = q.from(ServiceEntity.class);
        q.select(service);

        Expression<ProviderEntity> provider = service.get("provider");
        Predicate predicate = cb.equal(provider, providerEntity);
        q.where(predicate);

        List<ServiceEntity> services = em.createQuery(q).getResultList();

        return services;
    }

    public List<ServiceEntity> searchBoxForAnonymUsers(String searchedWord) {
        TypedQuery<ServiceEntity> nameQuery = em.createQuery("SELECT s FROM ServiceEntity s WHERE s.name LIKE :searchedWord OR s.shortDescription LIKE :searchedWord", ServiceEntity.class);
        List<ServiceEntity> foundServices = nameQuery.setParameter("searchedWord", "%" + searchedWord + "%").getResultList();

        return foundServices;
    }

    public List<ServiceEntity> searchBox(String searchedWord) {

        TypedQuery<ServiceEntity> nameQuery = em.createQuery("SELECT s FROM ServiceEntity s WHERE s.name LIKE :searchedWord OR s.shortDescription LIKE :searchedWord OR s.description LIKE :searchedWord", ServiceEntity.class);
        List<ServiceEntity> foundServices = nameQuery.setParameter("searchedWord", "%" + searchedWord + "%").getResultList();

        return foundServices;
    }
}
