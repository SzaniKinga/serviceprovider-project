package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.model.CategoryDAO;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {

    @Mock
    CategoryDAO dao;

    CategoryService service;

    public CategoryServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        dao = Mockito.mock(CategoryDAO.class);
        service = new CategoryService(dao);

        ServiceEntity se1 = new ServiceEntity();
        ServiceEntity se2 = new ServiceEntity();
        ServiceEntity se3 = new ServiceEntity();
        List<ServiceEntity> services = new ArrayList<>();
        services.add(se1);
        services.add(se2);
        services.add(se3);

        CategoryEntity ce1 = new CategoryEntity(1, "c1", services);
        CategoryEntity ce2 = new CategoryEntity(2, "c2", services);
        CategoryEntity ce3 = new CategoryEntity(3, "c3", services);

        List<CategoryEntity> categories = new ArrayList<>();
        categories.add(ce1);
        categories.add(ce2);
        categories.add(ce3);

        when(dao.getCategories()).thenReturn(categories);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testFindAll() throws SQLException {
        assertEquals(dao.getCategories().size(), service.getCategories().size());
    }

    @Test
    public void getCategoryById() {
        assertEquals(dao.getCategoryById(1), service.getCategoryById(1));
    }

}
