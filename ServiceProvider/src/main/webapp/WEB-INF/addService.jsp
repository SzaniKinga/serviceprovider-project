<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>Add new service</title>

        <style type="text/css">
            h2  {text-align: center;
                 padding: 10px;}
            </style>

        </head>
        <body>
            <%@include file="headlineAndSearch.jsp" %>

            <h2>Add new service</h2>
             <c:if test="${addServiceError != null}">
                <div class="alert alert-danger" role="alert">
                    ${addServiceError}
                </div>
            </c:if>
            <div class="container">
            <form method="POST" action="ServiceServlet" enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="Enter name" value="${name}" id="name" name="name" required>
                    <div class="invalid-feedback">
                        A valid name is required.
                    </div>
                </div>
                <div class="form-group">
                    <label for="shortDescription">Short description:</label>
                    <input type="text" class="form-control" placeholder="Enter short description (limited to 160 characters)" value="${shortDescription}" id="shortDescription" name="shortDescription" maxlength="160" required>
                    <div id="shortDescription_feedback"></div>
                    <div class="invalid-feedback">
                        Please enter a short description.
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea class="form-control" rows="7" style="height:25%;" placeholder="Enter description (limited to 560 characters" id="description" name="description" maxlength="560" required>${description}</textarea>
                    <div id="description_feedback"></div>
                    <div class="invalid-feedback">
                        Please enter a detailed description.
                    </div>
                </div>
                <div class="form-group">
                    <label for="image">Upload image:</label>
                    <input type="file" name="file">
                </div>
                <div class="form-group">
                    <label for="category">Category:</label>
                    <select id="category[]" name="category[]" class="form-control" multiple="multiple" required>                        
                        <c:forEach var="categoryDTO" items="${categoryDTOs}">
                            <c:set var="contains" value="false" />
                            <c:forEach var="category" items="${categories}">
                              <c:if test="${category eq categoryDTO.id}">
                                <c:set var="contains" value="true" />
                              </c:if>
                            </c:forEach>
                            <option value="${categoryDTO.id}" <c:if test="${ contains }">selected</c:if>>${categoryDTO.name}</option>
                        </c:forEach>
                        <option value="0">Other</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <form method="POST" action="CategoryServlet">
                <div class="form-group invisible" id="new-category">
                    <label for="new-category">New category:</label>
                    <input type="text" class="form-control" placeholder="New category name" name="name" required>
                    <button type="submit" id="create-category" class="btn btn-primary">Create category</button>
                </div>
            </form>
           
            <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>           
            <script>
                const categorySelect = document.getElementById('category[]');
                const newCategoryDiv = document.getElementById('new-category');

                categorySelect.addEventListener('change', (event) => {
                    if (event.target.value === '0') {
                        newCategoryDiv.classList.remove('invisible');
                    } else {
                        newCategoryDiv.classList.add('invisible');
                    }
                });
            </script>
            <script>
                $(document).ready(function () {
                    var text_max = 160;
                    $('#shortDescription_feedback').html(text_max + ' characters remaining');

                    $('#shortDescription').keyup(function () {
                        var text_length = $('#shortDescription').val().length;
                        var text_remaining = text_max - text_length;

                        $('#shortDescription_feedback').html(text_remaining + ' characters remaining');
                    });
                    $('#shortDescription').keyup();
                });
            </script>
            <script>
                $(document).ready(function () {
                    var text_max = 560;
                    $('#description_feedback').html(text_max + ' characters remaining');

                    $('#description').keyup(function () {
                        var text_length = $('#description').val().length;
                        var text_remaining = text_max - text_length;

                        $('#description_feedback').html(text_remaining + ' characters remaining');
                    });
                    $('#description').keyup();
                });
            </script>
    </body>
</html>
