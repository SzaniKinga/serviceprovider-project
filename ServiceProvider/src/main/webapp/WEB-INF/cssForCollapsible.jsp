<style>
    .collapsible {
        background-color: #777;
        color: white;
        cursor: pointer;
        padding: 18px;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
    }

    .active, .collapsible:hover {
        background-color: #555;
    }

    .content {
        color: black;
        padding: 0 18px;
        display: none;
        overflow: hidden;
        background-color: lightgrey;
    }

    @media (min-width: 576px) {
        .card-columns {
            column-count: 1;
        }
    }

    @media (min-width: 768px) {
        .card-columns {
            column-count: 2;
        }
    }

    @media (min-width: 992px) {
        .card-columns {
            column-count: 2;
        }
    }

    @media (min-width: 1200px) {
        .card-columns {
            column-count: 3;
        }
    }

    #button1{
        width: 150px;
        height: 40px;
        text-align:center;

    }
    #providerId{
        width: 150px;
        height: 40px;
        text-align:center;
    }
    #serviceId{
        width: 150px;
        height: 40px;
        text-align:center;
    }    
    #id{
        width: 150px;
        height: 40px;
        text-align:center;
    }
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 6px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        font-style: italic;
        font-weight: bold;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
    }
    .button4 {
        background-color: lightgrey;
        color: black;                
    }

    .button4:hover {background-color: lightgrey;
    }
</style>
