/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.serviceprovider.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ServiceDTO {
    
    private int id;
    private String name;
    private String shortDescription;
    private String description;
    private String image;
    private ProviderDTO providerDTO;
    
    @EqualsAndHashCode.Exclude
    private List<CategoryDTO> categoryDTOs;
    
}
