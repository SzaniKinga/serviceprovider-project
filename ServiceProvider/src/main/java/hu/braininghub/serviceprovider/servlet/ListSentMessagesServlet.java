package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.MessageDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.MessageService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListSentMessagesServlet", urlPatterns = {"/ListSentMessagesServlet"})
public class ListSentMessagesServlet extends HttpServlet {

    @Inject
    private MessageService messageService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {

            List<MessageDTO> messageDTOs = messageService.getSentMessages(user.getEmail());
            request.setAttribute("messageDTOs", messageDTOs);
            request.getRequestDispatcher("WEB-INF/listSentMessages.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {

            if (request.getParameter("deleteMessageButton") != null) {
                try {
                    Long messageId = Long.parseLong(request.getParameter("deleteMessageButton"));
                    if (messageService.getMessageById(messageId).getSender().equals(user.getEmail())) {
                        messageService.removeMessage(messageId);
                    }
                } catch (Exception ex) {
                    request.setAttribute("deleteMessageError", ex.toString());
                }
            }

            response.sendRedirect(request.getContextPath() + "/ListSentMessagesServlet");
        } else {

            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }
}
