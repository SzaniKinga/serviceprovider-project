package hu.braininghub.serviceprovider.model;

import hu.braininghub.serviceprovider.entity.MessageEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.LoggerFactory;

@Singleton
public class MessageDAO {

    @PersistenceContext
    private EntityManager em;

    public void addMessage(MessageEntity messageEntity) {

        try {
            em.persist(messageEntity);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Message successfully created");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error creating new message. Reason: {}", e.getMessage());
        }
    }

    public void removeMessage(Long id) {
        MessageEntity me = em.getReference(MessageEntity.class, id);
        try {
            em.remove(me);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Message successfully deleted");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("An error occured while deleting message. Reason: {}", e.getMessage());
        }

    }

    public MessageEntity getMessageById(Long id) {

        return em.find(MessageEntity.class, id);
    }

    public List<MessageEntity> getIncomingMessages(String recipient) {
        return em.createQuery("SELECT m FROM MessageEntity m WHERE m.recipient LIKE '" + recipient + "'").getResultList();
    }

    public List<MessageEntity> getSentMessages(String sender) {
        return em.createQuery("SELECT m FROM MessageEntity m WHERE m.sender LIKE '" + sender + "'").getResultList();
    }
}
