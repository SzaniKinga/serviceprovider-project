package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.LoggerFactory;

@WebServlet(name = "RemoveServiceServlet", urlPatterns = {"/RemoveServiceServlet"})
public class RemoveServiceServlet extends HttpServlet {

    @Inject
    private ServiceService serviceService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();

        final String id = request.getParameter("serviceId");

        if (id != null) {
            try {
                int serviceId = Integer.parseInt(id);
                serviceService.removeService(serviceId);
            } catch (Exception ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("A number is required");
                session.setAttribute("removeError", ex.getMessage());
            }

        }

        response.sendRedirect(request.getContextPath() + "/ListServicesServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
