/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.serviceprovider.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import javax.servlet.http.Part;
import org.slf4j.LoggerFactory;

public class UploadHelper {

    final String imageDirName = "images";
    String pathToPersist = null;

    public String getPathToPersist() {
        return pathToPersist;
    }
    
    public String getImageDirName() {
        return imageDirName;
    }
    
    public String uploadFile(String realPath, Part filePart) {

        final String imagePath = realPath + imageDirName;
        final File imageDir = new File(imagePath);

        String fileName = null;
        
        try {
            if (!imageDir.exists()) {
                imageDir.mkdir();
            }

            fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            InputStream fileContent = filePart.getInputStream();

            String imageFullPath = imagePath + "/" + fileName;
            File targetFile = new File(imageFullPath);

            byte[] buffer = new byte[fileContent.available()];
            fileContent.read(buffer);
            try (OutputStream outStream = new FileOutputStream(targetFile)) {
                outStream.write(buffer);
            } catch (IOException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("Fájlhiba");
            }
        } catch (IOException ex) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Fájlhiba");
        }
        pathToPersist = imageDirName + "/" + fileName;
        return pathToPersist;
    }
}
