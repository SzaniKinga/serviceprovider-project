package hu.braininghub.serviceprovider.entity;

import hu.braininghub.serviceprovider.helper.NotifySamples;
import hu.braininghub.serviceprovider.service.EmailService;
import javax.inject.Inject;
import javax.persistence.*;
import org.slf4j.LoggerFactory;

public class MessageEntityListener {
    
    @Inject
    EmailService notifyService;
    
    @PostPersist
    void onPostPersist(MessageEntity entity) {
        LoggerFactory.getLogger(this.getClass().getName()).debug("Sending email notification to: {} from: {}", entity.getRecipient(), entity.getSender());
        notifyService.sendEmailNotification(entity.getRecipient(), NotifySamples.NEW_MESSAGE_SUBJECT , NotifySamples.NEW_MESSAGE_TEXT);
    }    
}
