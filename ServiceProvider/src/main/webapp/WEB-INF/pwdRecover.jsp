<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <%@include file="cssForHeadLine.jsp" %>
        <%@include file="head.jsp" %>
        <title>Password Recovery</title>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>
        <div class="container">
            <h1>Awww, you forgot you password? :(</h1>
            <form method="POST" action="PasswordRecoveryServlet" class="was-validated">
                <div class="form-group col-6">
                    <label for="email">Enter your registered email:</label>
                    <input type="email" class="form-control" value="${emailAttempt}" id="email" name="email" required>
                </div>
                <button type="submit" class="btn btn-outline-dark">Send me new password</button>
            </form>
            <c:if test="${newPwdError != null}">
                <div class="alert alert-danger" role="alert">
                    ${newPwdError}
                </div>
            </c:if>
        </div>
        <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>
    </body>
</html>
