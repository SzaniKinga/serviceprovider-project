package hu.braininghub.serviceprovider.model;

import hu.braininghub.serviceprovider.entity.ProviderEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.LoggerFactory;

@Singleton
public class ProviderDAO {

    @PersistenceContext
    private EntityManager em;

    public ProviderEntity getProviderById(int id) {
        ProviderEntity p = em.find(ProviderEntity.class, id);
        return p;
    }

    public ProviderEntity getProviderByEmail(String email) {
        try {
            TypedQuery<ProviderEntity> query = em.createQuery("SELECT p FROM ProviderEntity p WHERE p.email = :email", ProviderEntity.class);
            ProviderEntity p = query.setParameter("email", email).getSingleResult();
            LoggerFactory.getLogger(this.getClass().getName()).debug("Provider successfully found by {} email", email);
            return p;
        } catch (NoResultException e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Provider could not be found. Reason: {}", e.getMessage());
            return null;
        }
    }

    public List<ProviderEntity> getProviders() {
        List<ProviderEntity> providers = em.createQuery("SELECT p FROM ProviderEntity p").getResultList();
        
        return providers;
    }

    public void addNewProvider(ProviderEntity provider) {
        
        try {
            em.persist(provider);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Provider successfully created");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error creating new provider. Reason: {}", e.getMessage());
        }
    }
}
